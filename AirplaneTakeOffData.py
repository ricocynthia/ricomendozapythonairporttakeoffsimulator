# AirplaneTakeOffData.py
# A class that represents each of the airplane's take off data
# needed to queue each plane for take off
# 
# Cynthia Rico Mendoza

class AirplaneTakeOffData:

    # Setting the parameters as the labled variables to help keep track
    # of each plane's take off data and requests 
    def __init__(self, ID, timeSubmission, timeSlotReq, lengthOfTimeReq):

        self.ID = ID
        self.timeSubmission = timeSubmission
        self.timeSlotReq = timeSlotReq
        self.lengthOfTimeReq = lengthOfTimeReq


    # Getter method for plane ID
    def getID(self):
        return self.ID

    # Setter for plane ID
    def setID(self, ID):
        self.ID = ID

    # Getter method for time submission
    def getTimeSubmission(self):
        return self.timeSubmission
    
    # Setter for time submission
    def setTimeSubmission(self, timeSubmission):
        self.timeSubmission = timeSubmission

    # Getter for time slot request (start time)
    def getTimeSlotReq(self):
        return self.timeSlotReq

    # Setter for time slot request (start time)
    def setTimeSlotReq(self, timeSlotReq):
        self.timeSlotReq = timeSlotReq
    
    # Getter for length of time requested (duration)
    def getLengthOfTimeReq(self):
        return self.lengthOfTimeReq

    # Setter for length of time requested (duration)
    def setLengthOfTimeReq(self, lengthOfTimeReq):
        self.lengthOfTimeReq = lengthOfTimeReq

    # Getter for actual time start
    def getActualTimeSlot(self):
        return self.actualTimeSlot

    # Setter for actual time start
    def setActualTimeSlot(self, actualTimeSlot):
        self.actualTimeSlot = actualTimeSlot
    
    # Getter for actual end time
    def getActualEndTime(self):
        return self.actualEndTime

    # Setter for actual end time
    def setActualEndTime(self, actualEndTime):
        self.actualEndTime = actualEndTime
    
    # Prints all the self variables of the AirplaneTakeOffData
    def getAllTakeOffData(self):
        return self.ID, self.timeSubmission, self.timeSlotReq,  self.lengthOfTimeReq
