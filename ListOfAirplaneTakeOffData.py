from AirplaneTakeOffData import *

# ListOfAirplaneTakeOffData.py
# A class that has a list of the AirplaneTakeOffData for each airplane ready 
# for take off. The AirplaneTakeOffData objects are added as the input 
# file is being opened and read.
# 
# Cynthia Rico Mendoza

class ListOfAirplaneTakeOffData:
    listOfAirplanesForTakeOff = []

    # Read through input file based on input path name
    def __init__(self, path):
        self.readInputFileData(path)

    # Read through the path file and save data
    def readInputFileData(self, path): 

        # Open file
        fooFile = open(path, 'r')

        # Read file
        fileRead = fooFile.read()

        # Saving the input file to a data array (string values)
        # splitlines() puts input data in array by line
        data = fileRead.splitlines()

        # Save input file data to the airplanesTakeOffData[] with each corresponding field
        # Go through each string (index) of the data array and save each chars before 
        # the commas as the label variables
        i = 0
        while i < len(data):

            # Save the chars before the first comma as the ID
            ID = data[i].rsplit(',')[0]

            # Turning string values to int to set the variables as numbers
            timeSubmission = int(data[i].rsplit(',')[1])
            startTimeReq = int(data[i].rsplit(',')[2])
            durationReq = int(data[i].rsplit(',')[3])

            # Creating a new AirplaneTakeOffData class with the input file data
            takeOffData = AirplaneTakeOffData(ID, timeSubmission, startTimeReq, durationReq)

            # Add the AirplaneTakeOffData class to the airplanesTakeOffData
            self.listOfAirplanesForTakeOff.append(takeOffData)
            i+=1

    # Allows user to print and read through the list of airplanes ready for take off
    def printListOfAirplaneTakeOff(self):
        i = 0
        while i < len(self.listOfAirplanesForTakeOff):
            print(self.listOfAirplanesForTakeOff[i].getAllTakeOffData())
            i += 1


    # Returns the list listOfAirplanesForTakeOff
    def getListOfAirplanesForTakeOff(self):
        return self.listOfAirplanesForTakeOff


