from ListOfAirplaneTakeOffData import *
from SortAirplaneTakeOff import *

# Main.py
# 
# Cynthia Rico Mendoza
def main():

    # Path of file
    path = 'C:/Users/ricom/Documents/RicoMendozaPythonAirportTakeOffSimulator/planes.txt'
    
    # Send path of file to ListOfAirplaneTakeOff data to create a list with each of 
    # the airplane's take off information 
    readInputFile = ListOfAirplaneTakeOffData(path)
    listOfAirplaneTakeOffs = readInputFile.getListOfAirplanesForTakeOff()

    # Create SortAirplaneTakeOff to sort the take off data by start time requests
    sortTakeOffList = SortAirplaneTakeOff(listOfAirplaneTakeOffs)

    # Print sorted airplane take off schedule
    sortTakeOffList.printReadyForTakeOff()

if __name__ == '__main__':
    main()