Airport take-off time slot simulation in Python.

In this project we will be keeping track of the information needed to schedule the airstrip resource.
All of which will be handled in a class to hold the following information:
    -Request identifier
    -Request submission time
    -Time slot requested
    -Length of time requested
    -Actual time start
    -Actual end start

We will also need a queue of airplanes, which are waiting before they can take off, and be able to print out 
the status of the queue as time moves along.

Here is an example of what an input file could look like:
    ID, Submission Time, Requested Start, Requested Duration
    Delta 160, 0, 0, 4
    UAL 120, 0, 5, 4
    Delta 6, 2, 3, 6

    At time 0 the queue would look like: Delta 160 (started at 0), UAL 120 (scheduled for 5)
    At time 1 the queue would look like: Delta 160 (started at 0), UAL 120 (scheduled for 5)
    At time 2 the queue would look like: Delta 160 (started at 0), Delta 6 (scheduled for 4), UAL 120 (scheduled for 10)
    At time 3 the queue would look like: Delta 160 (started at 0), Delta 6 (scheduled for 4), UAL 120 (scheduled for 10)
    At time 4 the queue would look like: Delta 6 (started at 4), UAL 120 (scheduled for 10)

At the end, we will be able to print out a listing of the actual take off times:
    Delta 160 (0-3), Delta 6 (4-9), UAL 120 (10-13)

Iteration One:
    I was able to create a AirplaneTakeOffData class, where the it stores the data of the airplane (ID, time submisson, time slot req, and duration - the actual time slot and end time will be set up for the second iteration). This was done to help with organization of the list, so each index of the list is going to have an AirplaneTakeOffData class stored in there (with everything we need). The ListOfAirplaneTakeOffData is where the reading of the file is done and then begins to store the AirplaneTakeOffData classes with the input file values. The SortAirplaneTakeOff class, currently, has the functionality of sorting the list of AirplaneTakeOffData classes by time submission. 

    In main I checked to see if the readyToTakeOffList has the airplaneTakeOffData in order by time submission by calling the print function that I made.
    
Iteration Two:
    I was able to create a method for sorting the AirplaneTakeOffData based on the time slot requests. I used the same code/concept as the method for sorting by time submission. I found that I didn't really use/need the sortByTimeSubmission method, since the list was based on the time slot values. I was confused as to how the time submisson was going to be handled. I may have taken the wrong approach if that is the case... I also created a print method to print the airplane ID, actual start time, and actual end time. 