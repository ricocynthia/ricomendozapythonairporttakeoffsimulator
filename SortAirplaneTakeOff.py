from ListOfAirplaneTakeOffData import *
from AirplaneTakeOffData import *

# SortAirplaneTakeOff.py
# A class that sorts the airplane take off data based on requested 
# start time and adds it to the queue for take off
# 
# Cynthia Rico Mendoza

class SortAirplaneTakeOff:

    # Initializing the ready for take off list so we can add the planes in order
    readyForTakeOff = []

    def __init__(self, listOfAirplaneTakeOffData):
        self.listOfAirplaneTakeOffData = listOfAirplaneTakeOffData
        self.sortAirplaneTakeOffData(self.listOfAirplaneTakeOffData)

    # Sorts the airplane data by time submissons and time slot requests
    # Then prints the airplane ID and actual start and end time
    def sortAirplaneTakeOffData(self, listOfAirplaneTakeOffData):
        self.sortByTimeSubmission()
        self.sortByTimeRequest()

    # This method sorts the list of airplane take off data by the time slot requests
    def sortByTimeRequest(self): 

        # Creating lists with unordered and ordered time slot requests
        rawTimeSlotRequests = self.getTimeSlotRequests()
        sortedTimeSlotRequests = self.getSortedTimeSlotRequests(rawTimeSlotRequests)

        i = 0
        while i < len(sortedTimeSlotRequests):

            # Get each airplane time slot request
            # Creating an array that olds the indices of where the current time slot request is 
            # in the listOfAirplaneTakeOffData list
            currentValue = sortedTimeSlotRequests[i]
            indicesOfValue = self.getIndicesOfEachValue(currentValue, rawTimeSlotRequests)

            # Adding the AirplaneTakeOffData to the readyForTakeOff list in
            # min to max order of the time slot requests
            if len(indicesOfValue) == 1:
                index = indicesOfValue[0]
                self.addToAirplaneTakeOffList(self.listOfAirplaneTakeOffData[index])
                i+=1
            else:
                for ii in range(len(indicesOfValue)):
                    index = indicesOfValue[ii]
                    self.addToAirplaneTakeOffList(self.listOfAirplaneTakeOffData[index])
                    ii+=1
                i = i + (len(indicesOfValue)) 


    # Sorts the list of AirplaneTakeOffData from min to max based on the time submission values
    # I didn't really use this method, as I ended up sorting the airplanes based on the time request submissions
    # and found that I wasn't sure how else to use/solve this program with sorting by time submission
    def sortByTimeSubmission(self):
        rawTimeSubmissions = self.getTimeSubmissions()
        sortedTimeSubmissions = self.getSortedTimeSubmissions(rawTimeSubmissions)

        # Run through the sortedTimeSubmissions list
        i = 0
        tempAirplaneList = []
        while i < len(sortedTimeSubmissions):
            
            # Get each airplane duration time
            # find the other information that goes with time slots reqs
            currentValue = sortedTimeSubmissions[i]
            indicesOfValue = self.getIndicesOfEachValue(currentValue, rawTimeSubmissions)

            if len(indicesOfValue) == 1:
                index = indicesOfValue[0]
                tempAirplaneList.append(self.listOfAirplaneTakeOffData[index])
                i+=1
            else:
                for ii in range(len(indicesOfValue)):
                    index = indicesOfValue[ii]
                    tempAirplaneList.append(self.listOfAirplaneTakeOffData[index])
                    ii+=1
                i = i + (len(indicesOfValue))

    # Returns a list of the listlistOfAirplaneTakeOffDataOfAirplane's time slot requests
    # The listOfAirplane list contains AirplaneTakeOffData objects
    def getTimeSlotRequests(self):

        # Local list to store each of the airplane's time start requests
        timeSlotsRequested = []
        # Add each AirplaneTakeOffData's time slot request to the timeSlotsRequested[]
        i = 0
        while i < len(self.listOfAirplaneTakeOffData):
            timeSlotReq = self.listOfAirplaneTakeOffData[i].getTimeSlotReq()
            timeSlotsRequested.append(timeSlotReq)
            i += 1

        # sortedTimeSlotRequests = self.getSortedTimeSlotRequests(timeSlotsRequested)
        return timeSlotsRequested

    # Returns a sorted list of the time slot requests in the listOfAirplaneTakeOffData
    # The listOfAirplane list contains AirplaneTakeOffData objects
    def getSortedTimeSlotRequests(self, timeSlotsRequested):
        sortTimeSlots = sorted(timeSlotsRequested)

        return sortTimeSlots

    # Returns a list of the listlistOfAirplaneTakeOffDataOfAirplane's time submission
    # The listOfAirplane list contains AirplaneTakeOffData objects  
    def getTimeSubmissions(self):
        timeSubmissions = []

        i = 0
        while i < len(self.listOfAirplaneTakeOffData):
            timeSubmission = self.listOfAirplaneTakeOffData[i].getTimeSubmission()
            timeSubmissions.append(timeSubmission)
            i+=1
        
        # Call getSortedTimeSubmissions to sort timeSlotSubmissions in min order
        # sortedTimeSlotSubmissions = self.getSortedTimeSubmissions(timeSlotSubmissions)

        return timeSubmissions

    # Returns a sorted list of the time submissions in the listOfAirplaneTakeOffData
    # The listOfAirplane list contains AirplaneTakeOffData objects
    def getSortedTimeSubmissions(self, timeSubmissions):
        sortTimeSlotSubmissions = sorted(timeSubmissions)

        return sortTimeSlotSubmissions

    # Adds AirplaneTakeOffData to the readyForTakeOffData list
    def addToAirplaneTakeOffList(self, airplaneTakeOffData):
        self.readyForTakeOff.append(airplaneTakeOffData)

    # Gets the index of duplicated elements in list
    # Returns an array that hold the indices of currentValue in 
    def getIndicesOfEachValue(self, currentValue, rawData):
        indexValues = []
        timeSubmission = currentValue
        for index in range(len(rawData)):
            if rawData[index] == timeSubmission:
                indexValues.append(index)

        return indexValues

    # Prints the airplane take off data (ID, actual time slot, & actual end time)
    def printReadyForTakeOff(self):
        i = 0
        while i < len(self.readyForTakeOff):
            if i == 0:
                duration = self.readyForTakeOff[i].getLengthOfTimeReq()
                timeSlot = self.readyForTakeOff[i].getTimeSlotReq()
                self.readyForTakeOff[i].setActualTimeSlot(timeSlot)
                endTime = timeSlot + (duration - 1)
                self.readyForTakeOff[i].setActualEndTime(endTime)
                ID = self.readyForTakeOff[i].getID()
                print(ID, "({timeSlot}-{endTime})".format(timeSlot=timeSlot, endTime=endTime))
                i+=1
            else:
                ii = i - 1
                duration = self.readyForTakeOff[i].getLengthOfTimeReq()
                timeSlot = self.readyForTakeOff[ii].getActualEndTime() + 1
                self.readyForTakeOff[i].setActualTimeSlot(timeSlot)
                endTime = timeSlot + (duration - 1)
                self.readyForTakeOff[i].setActualEndTime(endTime)
                ID = self.readyForTakeOff[i].getID()
                print(ID, "({timeSlot}-{endTime})".format(timeSlot=timeSlot, endTime=endTime))
                i+=1
        
